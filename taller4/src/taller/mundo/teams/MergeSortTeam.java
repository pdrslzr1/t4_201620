package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
    	 // Trabajo en Clase
         int l = lista.length;
         if (l < 2) return lista;
         int part = l/2;
         Comparable[] left = new Comparable[part];
         Comparable[] right = new Comparable[l-part];
         for(int i=0; i <part;i++)
         {
             left [i] = lista[i];
         }
         for(int i=part; i <l;i++)
         {
             right [i] = lista[i];
         }
         merge_sort(left,orden);
         merge_sort(right,orden);
         merge(right,left,orden);
         return lista;
     }

     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase
         int i,j,k;
         i = j = k = 0;
         int nL = izquierda.length;
         int nR = derecha.length;
         Comparable [] a = new Comparable[nL+nR];
         if(orden == TipoOrdenamiento.ASCENDENTE) {
             while (i < nL && j < nR) {
                 if (izquierda[i].compareTo(derecha[j]) == -1) {
                     a[k] = izquierda[i];
                     k++;
                     i++;
                 } else {
                     a[k] = derecha[j];
                     k++;
                     j++;
                 }
             }
         }
         else
         {
             while (i < nL && j < nR) {
                 if (izquierda[i].compareTo(derecha[j]) == 1) {
                     a[k] = izquierda[i];
                     k++;
                     i++;
                 } else {
                     a[k] = derecha[j];
                     k++;
                     j++;
                 }
             }
         }
    	 return a;
     }


}
