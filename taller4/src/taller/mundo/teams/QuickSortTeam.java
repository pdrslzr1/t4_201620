package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import taller.mundo.AlgorithmTournament;

import java.util.Arrays;
import java.util.Random;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

@SuppressWarnings("ALL")
public class QuickSortTeam extends AlgorithmTeam
{

     private static Random random = new Random();

     public QuickSortTeam()
     {
          super("Quicksort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
     {
          quicksort(list, 0, list.length, orden);
          return list;
     }
        // Trabajo en Clase
     
     private static void quicksort(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden)
     {


         if(inicio >= fin)
         return;
         int pivot = randInt(inicio,fin);
         int particion = particion(lista,inicio, fin,orden,pivot);
         quicksort(lista,0, particion-1,orden);
         quicksort(lista,particion+1, fin,orden);
     }


    private static int particion(Comparable[] lista, int inicio, int fin, TipoOrdenamiento orden,int pivot)
    {
        int left = inicio-1;
        int right = fin;
        while(left < right){
            while(lista[++left].compareTo(lista[pivot])==1);
            while(right > 0 && lista[--right].compareTo(lista[pivot])==1);
            if(left >= right){
                break;
            }else{
                Comparable c = lista[left];
                lista[left] = lista[right];
                lista[right]=c;
            }
        }
        Comparable c = lista[left];
        lista[left] = lista[right];
        lista[right]=c;
        return left;
    }

    /**
      Retorna un número aleatorio que se encuentra en el intervalo [min, max]; inclusivo.
      @param min, índice inicial del intervalo.
      @param max, índice final del intervalo.
      @return Un número aleatorio en el intervalo [min, max].
    **/
    public static int randInt(int min, int max) 
    {
          int randomNum = random.nextInt((max - min) + 1) + min;
          return randomNum;
    }
    // Trabajo en Clase

}
